00:00:01 - 00:00:12
Interviewer: Dankjewel en leuk dat ik jou mag interviewen en
dat je de tijd neemt voor dit interview. Je bent 
vrijwilliger bij de voorleesexpress? 

00:00:12 - 00:00:13
Respondent: Ja, klopt. 

00:00:13 - 00:00:15
Interviewer: Hoe lang ben je al vrijwilliger? 

00:00:15 - 00:00:17
Respondent: Vanaf 1 januari dit jaar. 

00:00:18 - 00:00:19
Interviewer: Nog vers! 

00:00:19 - 00:00:22
Respondent: Nog vers. Ik ben nu bij mijn tweede gezin. 

00:00:22 - 00:00:28
Interviewer: En kun jij iets vertellen over wat de 
voorleesexpress inhoudt? 

00:00:29 - 00:01:06
Respondent: De voorleesexpress houdt in, dat je met name de 
ouders gaat stimuleren dat ze gaan lezen en de kinderen 
meenemen in het plezier om te lezen. De praktijk laat wel 
zien dat het anders is. Ik kom ook in gezinnen waar ouders 
de taal niet machtig zijn en dan probeer ik op een andere 
manier het plezier in lezen door te geven, maar dan is het 
moeilijker om de ouders te stimuleren, dat is mijn ervaring.

00:01:07 - 00:01:14
Interviewer: En kun je eens iets vertellen hoe je dat doet? 
Hoe probeer jij zo'n gezin, of de ouders, te stimuleren? Wat
doe je dan precies? 

00:01:14 - 00:03:39
Respondent: Wat ik doe is, ik neem boeken mee, nadat ik 
kennis heb gemaakt met de kinderen. Ik maak een inschatting,
wat zou kunnen passen, kunnen aanspreken. Zoals het eerste 
gezin waar ik kwam, het meisje was vijf jaar en ze was heel 
erg gek op dieren, dus ik heb vooral dierenboeken gekozen. 
Het was een gezin waar met name de oudste broer bij de 
gesprekken aanwezig was. En moeder was er af en toe bij, 
want die oudste broer sprak Nederlands, maar moeder niet. Ik
kon haar een beetje verstaan. En de oudste broer heeft de 
rol op zich genomen, om 's avonds met het zusje te gaan 
lezen. Wat ik heb gedaan om het hele gezin te stimuleren, 
want het waren acht kinderen, is dat ik spelletjes heb 
meegenomen, memory met voorwerpen. En moeder ging daaraan 
meedoen, dus zij leerde ook zo op die manier de woorden en 
de beelden. En er was in dat gezin enorm veel plezier om 
samen spelletjes te doen. Dus ik heb een vaste opbouw in dat
uur, want je bent er een uur. Ik sloot daarmee af, want op 
een gegeven moment was het zo dat als ik kwam, dan wilde ze 
alleen die spelletjes doen. En ik denk: ja, dat vind ik wel 
leuk, maar heb ik er een structuur ingebouwd, dat we dat het
laatste kwartier doen en dat was wel heel leuk. En moeder 
vond het ook steeds leuker, ging ook kijken in de boekjes. 
En in dat gezin was het zo, ik vond dat moeder ook in haar 
eigen taal, in het Arabisch kon voorlezen. Maar dat wilden 
ze als gezin niet, ze wilden per se de Nederlandse taal, dat
ze dat spraken, ook in het gezin. Dus ze hebben moeder 
daarin ook niet de ruimte gegeven, met name de oudste zoon, 
om in het Arabisch voor te lezen. Hij zei: wij willen graag 
dat er Nederlands wordt geleerd. Dat heb je dan te 
respecteren natuurlijk, dus dan zet je meer in op het lezen 
van de broer met het jongste zusje, dan van moeder. Dat was 
wel de bedoeling geweest van de voorleesexpress. Maar goed, 
het plezier en het samen bezig zijn met spelletjes doen, wat
ze niet kenden, is ook gestimuleerd. Dus dat was heel leuk. 

00:03:39 - 00:03:40
Interviewer: En hoe oud was die broer dan? 

00:03:41 - 00:03:53
Respondent: Die broer was negentien. Hij was al langer hier 
in Nederland. Hij was twee jaar eerder in Nederland dan de 
rest van het gezin, waardoor hij ook de taal beter sprak. 

00:03:53 - 00:03:59
Interviewer: En jij zegt: acht kinderen. Die man van 
negentien was dan de oudste? 

00:03:59 - 00:04:32
Respondent: Ja, ik zeg: hij is de oudste. Hij had nog een 
oudere broer, die was 21 en hij had nog tweelingzusjes van 
20 en hij kwam daar dus onder. En er waren nog twee kinderen
van 9 en 8, en het meisje waarvoor ik kwam, er was ook nog 
een baby, maar dat meisje is dus geboren in Nederland en de 
anderen zijn allemaal uit Syrië, het is een Syrisch gezin. 
Ik zeg Syrisch, ze kwamen uit Irak. Ik kom nu een Syrisch 
gezin. 

00:04:35 - 00:04:41
Interviewer: En jij ging dan voor het meisje van vijf jaar, 
maar ik kan me voorstellen dat de iets oudere kinderen er 
ook bij zaten, of niet? 

00:04:41 - 00:05:12
Respondent: Ja, die zaten erbij, niet altijd, maar met name 
dat meisje van negen vond het heel leuk om erbij te zijn en 
zij ging ook boeken lenen. Ze gingen een kaart halen in de 
bibliotheek en zij ging ook boeken halen. Ik moet zeggen, 
vader was ook erg gemotiveerd, die ging dan op z'n fiets 
naar de bibliotheek, de boeken halen. En dat meisje van 
negen las ook en liet mij ook zien waar ze mee bezig was, 
dus dat was 
wel heel leuk. Ik had wel geboft met dat gezin, moet ik je 
zeggen. 

00:05:12 - 00:05:14
Interviewer: Dat ze zo gemotiveerd waren? 

00:05:14 - 00:05:17
Respondent: Ze waren heel gemotiveerd, ja. 

00:05:18 - 00:05:24
Interviewer: Mooi! En jij zegt: ze wilden het liefst 
spelletjes doen, maar op een gegeven moment was dat het 
laatste kwartier. Wat deed je dan die drie kwartier ervoor? 

00:05:25 - 00:06:26
Respondent: Ja, ik nam boeken mee, wat ik nu ook heb gedaan 
en ik liet dat meisje de boeken kiezen, wat zij wilde. En 
echt voorlezen, daar was zij aanvankelijk niet aan toe. Zij 
wilde zelf vertellen wat ze zag, dus dat heb ik gedaan. Ik 
heb die tekst helemaal losgelaten en ik heb haar laten 
vertellen wat ze zag en gaandeweg de tijd ben ik wel wat 
meer gaan lezen, omdat je natuurlijk toch de grenzen wat 
meer uitbreidt. En de jongen die tolkte 
deed zelf een opleiding als leraar en hij kwam met het idee,
en dat heb ik op een gegeven moment gevolgd, om het zusje de
zinnen te laten nazeggen, zodat ze de taal wat ging 
beheersen. Dus er zat echt zo'n opbouw in die 20 weken. Dat 
hebben we de laatste periode gedaan. 

00:06:27 - 00:06:32
Interviewer: Mooi, dus je kijkt wel met voldoening terug op 
dat gezin? 

00:06:32 - 00:06:53
Respondent: Ja, en ik zie vader nog wel eens op het 
voetbalveld, want zijn zoon en mijn kleinzoon voetballen. En
dan vertelt hij, dat ze het lezen hebben opgepakt. Maar het 
is met name de oudere kinderen die dan voorlezen. Dus van 
vader had ik ook niet erg de indruk dat hij ging lezen, maar
de andere kinderen wel. 

00:06:54 - 00:07:00
Interviewer: En wat was jouw verwachting van de 
voorleesexpress? Of wat is jouw verwachting? 

00:07:01 - 00:07:59
Respondent: Ik heb geen verwachting, ik stap in een gezin en
ik kijk wat daar mogelijk is. Ik ben nu in een gezin waar 
heel weinig mogelijk is. Moeder zou hoogopgeleid zijn in 
Syrië. Niet dat ze dom is, maar echt hoog opgeleid, ze zou 
een universitaire opleiding hebben, nou, die indruk heb ik 
niet. Ik heb er ook geen beeld van, ik kan dat niet 
vergelijken met hier. Ik zie wel dat moeder heel erg 
gemotiveerd is, maar moeder is ook ernstig getraumatiseerd, 
dat zie ik ook en dat blokkeert. En ik zie dat de kinderen, twee 
kleinere kinderen, ook last hebben van hetgeen ze hebben 
meegemaakt. Moeder is ziek geworden, na het vluchten hier 
naartoe. In Nederland heeft ze kanker gekregen, dus die 
kinderen hebben het heel zwaar gehad en dat merk ik. 

00:08:00 - 00:08:01
Interviewer: En waar merk je dat dan aan? 

00:08:02 - 00:09:07
Respondent: Dat ze heel druk zijn, ze kunnen heel weinig, ze
kunnen niet op de stoel zitten. Moeder had aanvankelijk 
bedacht dat ik op de bank ging voorlezen. Dat zit ook zo 
leuk, dat beeld wat je dan hebt. Maar dat werkte niet. Ik 
kom met name voor het zoontje van vijf en hij wordt in 
februari al zes. Maar hij kan niet de rust opbrengen om te 
zitten en zeker niet op een bank, want dan ziet hij veel te 
veel. Dus bij het laatste gesprek ben ik aan de hoge tafel 
gaan zitten, wat meer een werkhouding is en dat werkte goed.
Ik heb hem eerst laten tekenen, want dat doet hij graag, hij
wordt dan wat rustiger. Maar het is een manneke, dat heb ik 
ook al aangegeven, waarmee denk ik wel wat meer mee aan de 
hand is. Moeder ging op een gegeven moment een paar keer 
eventjes naar buiten en dan raakte hij in paniek. Ik kom uit
de psychiatrie en ik zie wat ik zie: dat manneke is bang dat
moeder niet terugkomt en dan zit je met zo'n vreemde mevrouw
in huis. 

00:09:08 - 00:09:09
Interviewer: Wat heftig! 

00:09:09 - 00:10:09
Respondent: Ja, dat vind ik wel heftig. Dus ik kan niet 
zeggen, van: nou, ik ga met een plan daar naartoe. Nu heb ik
ook boeken uitgezocht, waarvan ik denk: dit manneke zit in 
groep één op de kleuterschool, hij zit ook nu pas op school,
hij heeft alleen maar in de opvang gezeten, in die tijd dat 
moeder ziek was. Dus ik ga proberen te kijken, van: kan hij 
wat met letters, met tellen, want gezien zijn leeftijd zou 
dat wel al moeten kunnen, dat kan hij ook, dat ik hem 
daarmee kan stimuleren. Maar het is steeds millimeter werk, 
want hij heeft een heel kort concentratievermogen. Dus dan 
denk ik, van: ik neem boekjes mee, maar ik laat hem niet 
kiezen, want hij wordt overspoeld als hij teveel boekjes 
krijgt, dus ik selecteer. Dat is totaal het 
tegenovergestelde in dat andere gezin. 

00:10:09 - 00:10:15
Interviewer: Mooi dat je dat zo goed aanvoelt en dat je daar
helemaal op aanpast wat je doet! 

00:10:15 - 00:10:51
Respondent: Ja, ik heb natuurlijk ook vanuit mijn werk, ik 
kom vanuit de hulpverlening, dus dat is ook wat je doet in 
de hulpverlening. Je hebt wel een doel voor ogen, maar dat 
is heel ver weg. En om dat doel te bereiken, dat kan met een
heleboel zijwegen en met hulpacties. Want ik zit me nu ook 
te bedenken, ze hebben me nu drie weken niet gezien in dat 
gezin, dus ik ga morgen wel even een appje sturen dat ik 
eraan kom, want ze zouden me zomaar vergeten kunnen zijn. 

00:10:51 - 00:10:54
Interviewer: En hoe vaak ben je nu geweest? 

00:10:55 - 00:10:56
Respondent: Ik ben vier keer geweest. 

00:11:00 - 00:11:04
Interviewer: Wat denk je hoe het gaat? Zal er iets van een 
terugval zijn? 

00:11:05 - 00:12:54
Respondent: Deze moeder spreekt wel wat Nederlands, die 
verstaat mij ook, ze kan wel moeilijk Nederlands terug 
spreken, maar we kunnen elkaar wel begrijpen en zij wil ook 
per se gaan voorlezen. Dat doet ze ook. De kinderen, ze 
hebben een meisje van drie en het manneke waar ik voor kom, 
liggen samen op één kamer en zij vindt het heel moeilijk, 
omdat de kinderen samen naar bed gaan, dezelfde tijd, en 
voorlezen is dan heel moeilijk met hen alle twee. Want ze 
zijn alle twee onrustig, het meisje is ook onrustig, op een 
andere manier dan de jongen. Dus ik ben met moeder wat 
afspraken aan het maken, om eerst het meisje naar bed te 
brengen en met het zoontje beneden wat te lezen en ze 
probeert dat uit. En dat is een gezin met nog een zoon van 
zestien, daar is ze ook mee gevlucht naar Nederland, met die
jongen. Hij spreekt goed Nederlands, hij zit hier op de 
middelbare school. De laatste keer kwam hij er even bij 
zitten en toen zei hij tegen zijn moeder: mam, je probeert 
het wel, maar je spreekt ook woorden verkeerd uit. Ik moet 
het ook vaker wat corrigeren, dus dan denk ik, van: er is 
wel een bereidheid om vooruit te komen. Ook wel aandoenlijk 
dat hij dat zo zegt, maar je ziet een hele andere 
familieverhouding. Dat is natuurlijk altijd met mensen die 
uit die oorlogsgebieden komen. Was dat het antwoord op jouw 
vraag, want je vroeg hoe ik dat deed? 

00:12:54 - 00:12:59
Interviewer: Ja hoor. Als jij binnen zo'n gezin komt, zijn 
er dan nog dingen die je opvallen? 

00:12:59 - 00:14:41
Respondent: Ja, mij valt natuurlijk heel veel op. Ik heb 45 
jaar in de hulpverlening gezeten, ik ben ooit bij de 
kinderbescherming begonnen, dus er vallen me heel veel 
dingen op, maar ik leer ook heel veel van die gezinnen. Ik 
kom nu echt op een andere manier binnen. En dit gezin waar 
ik nu kwam, dat is echt een achterstandswijk. Ik bedoel, van
buitenaf zie je al rommel in de tuin, maar wat je ook ziet, 
is dat de buren heel betrokken zijn en je kunt dat 
bemoeizucht noemen, maar het gezin zelf ervaart het als heel
warme aandacht. Ik heb daar naar gevraagd, dat was niet mijn
taak als vrijwilligster, maar ik vind het toch belangrijk om
dat wel te benoemen. En toen zei moeder, dat ze zich heel 
erg opgevangen voelde, ook door de buren. En dat vind ik 
heel mooi om te zien. Het hebben over achterstandswijken, is
dat er ook veel meer betrokkenheid is. Er is veel meer 
betrokkenheid dan in mijn woonbuurt, zondermeer. Toen ik daar
de tweede keer kwam, was het heel warm weer en kwam de 
buurvrouw zeggen van: goh, ik heb het badje opgezet en dan 
kon het dochtertje van drie met haar dochtertje in het badje
en dan kon ik ook rustig aandacht geven. Ze wilde ook wel 
weten wie ik was, dus dat kon ik ook rustig zeggen. "O, wat 
fijn, dan leren ze ook goed lezen, nee is goed, dan komt dat
dochtertje bij mij, dan kun je met dat jongetje aan de 
slag." Dan denk ik, van: ja, dat is er ook, die aandacht. 

00:14:46 - 00:15:04
Interviewer: Mooi. In het onderzoek zijn wij ook heel erg 
geïnteresseerd in gedrag. Als we dan kijken naar het 
doelgedrag wat jullie willen bereiken met de 
voorleesexpress, wat is naar jouw beleving het doelgedrag 
waar jullie naar streven? 

00:15:04 - 00:15:43
Respondent: Ik denk met name dat ze de taal gaan beheersen, 
plezier krijgen in lezen. Want plezier hebben in lezen, is 
ook plezier krijgen in dingen te leren en om vooruit te 
komen. En dat is het allerbelangrijkste en bij beide 
gezinnen merk ik dat daar de behoefte is. Ik vind het ook 
heel goed van de voorleesexpress, dat ze de ouders heel 
duidelijk vragen of ze gemotiveerd zijn. En dat merk ik wel 
bij beide. Bij beide gezinnen is dat zeker aan de orde, want
ja, anders komen mensen niet vooruit. 

00:15:44 - 00:16:16
Interviewer: Als je kijkt naar twee doelgedragingen, als ik 
jou goed heb begrepen, is dat taalontwikkeling en ook 
taalplezier. Kun je die onderverdelen in deelgedragingen? Ik
heb voor mezelf een voorbeeld opgeschreven: als het 
doelgedrag is, bijvoorbeeld, gezond dieet, dan zou een 
deelgedrag kunnen zijn: je moet eerst gezonde ingrediënten 
kopen, dan moet je het nog kunnen koken. 

00:16:16 - 00:16:21
Respondent: Dat zijn allemaal stapjes die je moet zetten, om
daar te komen. 

00:16:22 - 00:16:33
Interviewer: Als je dan kijkt naar die twee doelgedragingen,
laten we dan de eerste even inzoomen: de taalontwikkeling. 
Wat doen jullie binnen de voorleesexpress? Wat zijn de 
stapjes daarin? 

00:16:33 - 00:18:53
Respondent: De stapjes zijn, dat je met name moeder vraagt 
om erbij te komen zitten, dat vind ik het eerste. Ik zeg 
tegen een moeder: ik geef een voorbeeld, hoe je met je kind 
kunt omgaan. Ik zeg niet, van: dat is de goede manier, maar 
je kunt zien hoe ik het doe. Ik laat dat ook zien als ik een
prentenboek openvouw, dan zeg ik tegen haar: ik ga niet de 
tekst lezen, ik laat de kinderen kijken en ik laat ze 
benoemen wat ze zien om hun aandacht te krijgen. Want de 
tekst is vaak nog te hoog gegrepen. Wat je zegt over de 
opbouw van een dieet, dat is dit dus ook, dat je eerst kijkt
naar: wat zie je daarin? En dat zeg ik tegen haar ook, want 
zij had aanvankelijk wel de neiging om te gaan lezen, dat 
begreep ik van die jongen van zestien. Ik zeg: vaak hoeft 
dat nog helemaal niet, of is dat nog stappen te ver. Je moet
eerst beginnen in het plezier krijgen, in de aandacht te 
krijgen en kinderen mee te nemen in wat ze zien. En ik heb 
bij dit manneke, waar ik nu kom, nog niet kunnen achterhalen
hoe dingen voor hem zijn, want hij heeft daar ook geen 
woorden voor. Ik weet ook niet of hij dat bewustzijn heeft. 
Het is ook een andere cultuur en de Nederlandse kinderen 
zijn zich daar veel meer bewust van, hoe dingen voor hen 
zijn, daar worden ze ook in opgevoed. Maar of dit manneke 
ook de vaardigheden heeft, weet ik niet. Dus dat is 
aftasten, dat is inderdaad heel basaal beginnen. Met een 
boekje en dar naar kijken. En ik vertaal ook aan moeder hoe 
mijn opbouw is en dat ik probeer tot een verhaal te komen. 
Ik had de laatste keer een boekje over de herfst meegenomen,
omdat het herfst is en ik had kastanjes meegenomen. Dat 
vonden de kinderen leuk en toen vertelden zij, dat het woord
kastanje in het Arabisch, bijna hetzelfde klonk als ons 
woord kastanje, dus dat was wel leuk en heb ik het daarover 
gehad. Ik probeer het allemaal wat te concretiseren. 

00:18:56 - 00:19:29
Interviewer: Heel mooi! Dan heb je allebei de 
doelgedragingen al een beetje benoemd, taalplezier en 
taalontwikkeling. Toch nog even op die twee ingaan. Wat zou 
je graag willen dat die gezinnen dadelijk gaan doen? Als jij
weg bent, wat zou dan in jouw ideale plaatje...? 

00:19:29 - 00:19:45
Respondent: Het ideale plaatje is, dat lezen een centrale 
plek in het gezin gaat krijgen. Dat het ook een rustmoment 
is, iets waar ze samen mee bezig zijn, waar ze elkaar kunnen
ontmoeten. Dat zou ik heel fijn vinden. 

00:19:46 - 00:19:47
Interviewer: Een soort voorleesritueel. 

00:19:47 - 00:20:19
Respondent: Ja. Ik heb verteld dat ik dat ook doe, met mijn 
eigen kinderen en mijn kleinkinderen en dan zeg ik: niet dat
je dat moet overnemen, maar ik heb er heel veel plezier aan 
beleefd, omdat je met de kinderen aan de slag ben. Ook dat 
ik met m'n eigen kinderen, ik heb drie kinderen, met ieder 
kind apart ging lezen. Niet samen, dat deed ik ook wel eens 
natuurlijk, maar ik ging met ieder kind apart lezen. Dat je 
dan één op één contact hebt, dat zou ik ze gunnen. 

00:20:19 - 00:20:26
Interviewer: Is dat trouwens, ik weet niet of je dat weet, 
in hun cultuur ook gebruikelijk om voor te lezen? 

00:20:27 - 00:22:23
Respondent: Nee. Het is meer verhalen vertellen, althans, 
dan heb ik het over de Arabische cultuur, wat in alle twee 
de gezinnen is. Maar, als ik kijk naar hun situatie, het is 
voor hen nog overleven, en zeker voor het gezin waar ik nu 
kom. In het andere gezin werd er wel meer gepraat, want er 
hingen ook foto's op van opa en oma uit Irak en daar heb ik 
naar gevraagd. In de periode dat ik daar kwam, was het Pasen
en zij hebben verteld hoe zij Pasen vieren. Ze vieren het 
ook op een andere manier. Ze geven wel aandacht aan 
rituelen. Maar het gezin waar ik nu kom, heb ik dat zicht 
niet zo. De partner van deze mevrouw waar ik kom, komt uit 
Iran. Hij komt dus niet uit Syrië, ze hebben elkaar in het 
asielzoekerscentrum leren kennen en hij is de vader van de 
twee jongste kinderen. Dus daar heb ik nog geen zicht op, of
zij de verhalen vertellen. Maar dat gezin uit Irak zeker 
wel, echt voorlezen, ook spelletjes, ook speelgoed. Bij dat 
eerste gezin, toen ik de laatste keer kwam, hadden ze 
gekookt voor mij en ik mocht iedereen meenemen uit mijn 
gezin. Maar goed, ik heb één kleindochter, ze is acht, en ze
was altijd heel erg nieuwsgierig naar het gezin waar ik toen
was. Ze wilde wel meegaan, dus ze heeft die middag gespeeld,
ook met dat meisje van negen. Dat ging heel leuk en toen zei
ik, van: ze hebben niet zo heel veel speelgoed. Zij is boven
geweest en ze zei: ze liggen allemaal samen op een kamer. 
Dus dat is een hele andere wereld, weer een hele andere 
cultuur. En boeken ook niet, ja, ze halen ze nu uit de 
bibliotheek. 

00:22:26 - 00:22:28
Interviewer: Ga je dan ook wel eens samen met het gezin 
boeken halen? 

00:22:28 - 00:23:05
Respondent: Heb ik nog niet gedaan. Bij dat gezin uit Irak, 
had ik dat gevraagd. Het waren Koerden en zij waren heel erg
op zichzelf. Dat doen we wel zelf, dat deden ze ook zelf. 
Want er waren ook middagen hier vanuit de voorleesexpress 
geregeld. Ik zeg: ik kan jullie ophalen, dan gaan we 
daarnaartoe. Nee, we gaan zelf daarnaartoe. Het was op een 
middag dat ik niet kon, maar zij zijn wel geweest, heb ik 
teruggehoord, dus dat doen ze wel. Dat gezin waar ik nu kom,
weet ik niet of die de mogelijkheden hebben, om dat te doen.
Dat is een wat armoediger gezin, vind ik. 

00:23:06 - 00:23:10
Interviewer: Ik kan me ook voorstellen, als je zoveel 
trauma's hebt, dat er misschien helemaal geen ruimte in het 
hoofd is. 

00:23:11 - 00:23:23
Respondent: Is er ook niet, merk ik. Moeder doet heel veel 
moeite, maar nee, ik hoop dat ze dat nog wel gaan 
ontwikkelen. 

00:23:27 - 00:23:38
Interviewer: Hoe denk jij dat voorlezen, of aan de hand van 
de prentenboek vertellen, bijdraagt aan taalontwikkeling en 
bijdraagt aan taalplezier? 

00:23:40 - 00:24:10
Respondent: Ik denk dat je door prentenboeken en vertellen 
en voorlezen, de wereld groter maakt voor kinderen en dat je
de bewustwording van een grotere wereld stimuleert en dat ze
vragen kunnen gaan stellen. Dat hoop ik. Het eerste gezin 
deed dat wel, ik hoop dat in dit gezin ook. Meer vragen 
stellen, daar leer je van, dat hoop ik. 

00:24:13 - 00:24:22
Interviewer: Kun je dan stellen dat taalontwikkeling en 
leesplezier middelen zijn om verder te komen? 

00:24:24 - 00:24:24
Respondent: Om verder te komen en verder te ontwikkelen. 

00:24:29 - 00:24:49
Interviewer: Ja, mooi. Denk jij nog dat er verschil is, dat 
bijvoorbeeld zo'n voorleesexpress, want daar hebben we het 
nu over, voor een bepaalde doelgroep effectiever zal zijn 
dan een andere doelgroep? Je bent noch niet zo heel lang 
vrijwilliger, maar misschien heb je er toch idee bij? 

00:24:57 - 00:26:04
Respondent: Ja, we hebben wel bijeenkomsten gehad, gelukkig 
ook met collega-vrijwilligers en dan valt me op dat de 
buitenlandse gezinnen, de vluchtelingen, die willen hier een
bestaan opbouwen. En ik vind het heel vervelend om te 
zeggen, maar onze Nederlandse gezinnen uit 
achterstandswijken, hebben een andere houding. Dat heb ik 
ook al van andere vrijwilligers gehoord. Ik heb het ook in 
mijn werk gezien, dat zij veel meer zitten in: ze moeten 
maar, ze moeten, veel meer consumerend. En dat is in ieder 
geval de ervaring die ik in deze gezinnen niet heb. Dat ze 
zoiets hebben, van: we willen deze kans die ons geboden 
wordt, grijpen. En ik denk, helaas, onze Nederlandse 
gezinnen in de achterstandswijken, waar veel kwaadheid is 
naar hun situatie, dat ze zich minder opbouwend presenteren.

00:26:10 - 00:26:16
Interviewer: Zou dan de Voorleesexpress juist wel of niet 
goed zijn voor die Nederlandse gezinnen? 

00:26:16 - 00:26:52
Respondent: Ik denk dat je het altijd moet aanbieden, dat je
het altijd moet aanbieden en dan blijkt wel of het, en hoe 
het werkt. En op het moment dat je dan niet meer investeert,
geef je het ook op, dat moet je niet doen, denk ik. Maar dat
je daar wel kritisch in bent. Dat heb ik ook wel eens van 
andere vrijwilligers gehoord, die zeggen, van: als ze niet 
zorgen dat het kind thuis is of dat het bezoek weg is, de 
voorwaarden niet creëren om aan de slag te kunnen, ja, dan 
moet je ook een grens stellen, want dat zijn wel de verhalen
die ik gehoord heb. 

00:26:52 - 00:26:57
Interviewer: Ja, het is ook de tijd van de vrijwilliger. 

00:26:57 - 00:27:23
Respondent: Het eerste gezin waar ik kwam, door de 
brievenbus zag ik al van de zwarte oogjes, die stonden te 
wachten tot het twee uur was. Dus dat is leuk en ik heb van 
een paar vrijwilligers gehoord, dat het niet zo was. Dan 
zeggen ze: o, daar komt die mevrouw weer. Ik zou daar niet 
20 weken vol maken. Ik zou het wel uitproberen 
en als het zo blijft, dan zou ik stoppen. 

00:27:23 - 00:27:30
Interviewer: Ja, want hoe werkt dat dan, want gezinnen die 
kunnen zelf kiezen om mee te doen, het is niet verplicht? 

00:27:30 - 00:28:07
Respondent: Ja, daar kies je voor, het is een contract wat 
het gezin aangaat met de voorleesexpress, dus er wordt van 
hen ook verwacht dat ze tijd, ruimte en dat er rust is, dat 
er gewerkt kan worden. Als ze aan die voorwaarden niet 
voldoen dan... Het is 20 keer dat je gaat. Ik heb ook 
ervaren dat het in het eerste gezin voldoende is geweest. In
het huidige gezin weet ik niet of dat voldoende is. 

00:28:08 - 00:28:09
Interviewer: Dan zou het misschien langer mogen? 

00:28:09 - 00:28:29
Respondent: Dan zou het misschien wel langer kunnen. Het 
manneke is een keer ziek geweest, ik heb een keer moeten 
afzeggen dat ik ziek was en nu is de vakantie ertussen. Dus 
het is geen aaneengesloten periode geweest. Juist in dit 
gezin is dat niet gelukkig. 

00:28:32 - 00:28:34
Interviewer: Ja, soms lopen dingen zoals ze lopen. 

00:28:34 - 00:28:35
Respondent: Ja, dat is zo. 

00:28:41 - 00:28:56
Interviewer: Toch nog even terug, ik weet dat jij nog geen 
gezinnen hebt gehad met de Nederlandse achtergrond, maar het
triggert mij wel. Ze mogen er aan meedoen, ze hoeven er niet
aan mee te doen en dat het dan toch blijkbaar lastig is. 
Waardoor zou dat komen, denk je? 

00:28:58 - 00:29:50
Respondent: Ja, ik denk, dan ga ik erg generaliseren, maar 
dat de Nederlandse cultuur wat makkelijker is, dat men het 
allemaal wel goed vindt als je aanbod krijgt. Het wordt 
allemaal gepresenteerd, het is een zorgcultuur hier. Mijn 
ervaring met mensen van het buitenland, is dat ze het niet 
vanzelfsprekend vinden, maar dan generaliseer ik. De 
meesten, wat ik zo merk van die bijeenkomst, komen in 
buitenlandse gezinnen. Er zijn ook heel veel drama's in 
Nederlandse gezinnen, echtscheidingen, huiselijk geweld. Ik 
denk dat dat ook blokkerend werkt. 

00:30:01 - 00:30:21
Interviewer: Is er voor jou nog verschil tussen spelletjes 
doen, taalspelletjes bedoel ik dan, voorlezen, laten 
vertellen, met prentenboeken bezig zijn? Misschien zijn er 
nog andere manieren. Zit daar voor jou nog verschil in, hoe 
die dingen bijdragen tot taalontwikkeling en het 
leesplezier? 

00:30:22 - 00:30:46
Respondent: Ik vind ze allemaal even belangrijk, in waarde 
vind ik het allemaal even belangrijk. Het gezin waar ik nu 
kom, omdat dat manneke zo druk was, ben ik de eerste keer 
begonnen met dingen te benoemen: haren, ogen, omhoog en 
omlaag, en dat is even waardevol als een boekje, denk ik, 

00:30:48 - 00:30:51
Interviewer: Ja, het is met de taal bezig zijn, daar gaat 
het om. 

00:30:51 - 00:31:17
Respondent: Gewoon bezig zijn en invoegen in zijn 
beweeglijkheid. Hij moest van mij springen en vooruitlopen 
en achteruit lopen. Dan spring je in op waar hij mee bezig 
is en krijg je hem tenminste mee. En ik moest wel zeggen of 
hij vooruit liep of achteruit liep, dan ben je dus toch met 
taal bezig. Ik denk dat dat even belangrijk is. 

00:31:18 - 00:31:29
Interviewer: Heb jij hier ook veel trainingen gevolgd? Want 
ik vind het heel knap dat jij al die verschillende vormen 
heel snel, o, dat kind heeft dit nodig en ik snap, je komt 
uit de hulpverlening, maar... 

00:31:29 - 00:32:11
Respondent: Nee, ik heb geen trainingen hier, we hebben geen
training hier gehad. Ik heb gevraagd of we ochtenden kunnen 
hebben, dat we bij elkaar kunnen komen, want je leert het 
meest toch van de andere vrijwilligers. En dat is mijn eigen
ervaring als hulpverlener, maar ook als moeder en als oma. 
Ik heb zes kleinkinderen die totaal verschillend zijn. Mijn 
drie kinderen zijn totaal verschillend. Mijn jongste dochter
kon ik met tien jaar al met de fiets de stad in laten gaan. 
Mijn zoon van dertien, daar hield ik nog mijn hart vast. Dus
daar pas je in aan. 

[aanvulling interviewee per mail d.d.: Na ons gesprek van 
afgelopen maandag viel me in, dat ik Schunck erg tekort 
heb gedaan met te zeggen dat we geen ondersteuning hadden. 
… Ik heb diverse “inspiriatiesessies” gehad, zoals vanmorgen. 
Er is wel degelijk begeleiding in die zin.
Ik ervaar die sessies niet als een cursus of zo, 
maar meer als begeleiding en contactmomenten met 
coördinatoren en andere vrijwilligers. 
Zou het fijn vinden wanneer je dit zou corrigeren bij 
de verwerking.] 

00:32:11 - 00:32:14
Interviewer: Ja, en die ochtend met vrijwilligers, is die er
gekomen? 

00:32:15 - 00:32:24
Respondent: Ja, we hebben er een paar gehad en we hebben het
aanstaande woensdag ook weer, dat is de derde keer dat ik 
het meemaak, geloof ik. Ik vind dat heel waardevol. 

00:32:24 - 00:32:26
Interviewer: Ja, dat kan ik me voorstellen, gewoon met een 
kop koffie? 

00:32:27 - 00:32:38
Respondent: Gewoon een kop koffie en wat vragen. Het wordt 
begeleid vanuit voorleesexpress, er worden wat vragen 
gesteld en daar kun je op reageren. Je leert gewoon heel 
veel van de anderen. 

00:32:39 - 00:32:51
Interviewer: Want jullie moeten ook heel kort een evaluatie 
invullen op het appje, na iedere sessie, of hoe moet ik het 
zeggen? Dat werkt, geloof ik, met smileys, maar je kunt ook 
nog tekst erbij schrijven? 

00:32:51 - 00:33:30
Respondent: Ik maakte er altijd wel een lange tekst bij, dat
begreep ik dus al van de coördinator van de VoorleesExpres. Maar 
voor mij is dat mijn werk geweest en het is mijn manier om 
dingen op een rij te zetten, van: waar ben ik bezig geweest?
Is meteen ook een werkpunt voor de volgende keer: o, dat is 
vandaag gebeurd, dat moet ik de volgende keer niet meer 
doen, bijvoorbeeld op de bank zitten. Ik denk: dat moet ik 
niet meer doen, ik moet hoog gaan zitten, in een werkhouding
zitten en dat valt me dan in als ik dat verslag aan het 
maken ben. Zo'n smiley zou voldoende kunnen zijn, maar 
voor mij is dat niet voldoende. Ik maak gewoon altijd een 
verslag. 

00:33:32 - 00:33:38
Interviewer: En je hebt drie of vier smileys, geloof ik? Heb
je wel eens een heel somber gezichtje gehad? 

00:33:38 - 00:35:17
Respondent: Ja, de voorlaatste keer heb ik een hele, want 
toen had de coördinator van de VoorleesExpress 
mij meteen ook gebeld. Ik had het 's avonds 
gemaakt en 's ochtends belde ze al meteen. "Dat moet toch 
verschrikkelijk zijn geweest?" Het manneke was echt 
onhandelbaar, het was erg. Maar de situatie was ook: vader 
ging zijn auto verkopen en ze vonden de autopapieren niet. 
Moeder ging daarmee aan de slag. Het manneke was 's morgens 
niet naar school geweest, omdat er studiedag was, dus hij 
had heel veel energie en was al vanaf half zes op. Hij was 
helemaal opgedraaid en ze hadden een nieuwe huishoudelijke 
hulp gehad en vervolgens kom ik nog een keer. Ik heb van 
alles geprobeerd en na drie kwartier denk ik: ik ga. 
Want de coördinator had
nog wel, van: ga je stoppen dan? En dat had moeder ook, van:
kom je nou niet meer? Ik zeg: jawel, ik kom nog wel. Het 
zijn zoveel omstandigheden voor een manneke, dat hij zo 
onrustig was. Ik had eens een keer een boek met een wc-rol, 
hij moest benoemen wat dat was. En dan gaat hij de kamer 
uit, hij gaat naar de wc tussendoor en dan komt hij na tien 
minuten terug. Was hij dus gaan poepen en komt hij terug met
dat wc papier. Ik moet dan ook wel lachen, dat is een heel 
leuk manneke. Ja, leuk. Maar dat was inderdaad de 
voorlaatste keer en toen heb ik het uur niet vol gemaakt. 
Nee, dat doen we het gezin niet aan en mezelf ook niet. 

00:35:18 - 00:35:20
Interviewer: En een heel vrolijke smiley? 

00:35:20 - 00:35:22
Respondent: Meestal wel. 

00:35:23 - 00:35:26
Interviewer: Wil je eens iets noemen, wat je heel erg leuk 
vindt, leuk vond gaan? 

00:35:27 - 00:36:11
Respondent: Dat wat ik heel leuk vond, dat was de laatste 
keer. Toen zei ik tegen dat manneke: zien we elkaar een paar
weken niet? En dat vond hij niet leuk, dat liet hij ook 
merken. Hij kwam naar mij toe om me vast te pakken en toen 
dacht ik: ja, dat vind ik wel heel erg leuk. We hebben wel 
contact, ja, maar ook van het begin af aan met hem. Hij 
heeft wat overeenkomsten met mijn zoon, dus ik herken daar 
dingen uit. Ik heb tegen moeder gezegd: ik herken het, 
ik ken ook 
je worsteling als moeder, want je moet wel tussendoor koken 
en je moet ook de wc poetsen en alles doen. Dan heb je zo'n 
stuiterbal de hele tijd thuis en het zusje is iets minder, 
maar kan er ook wat van. 

00:36:18 - 00:36:25
Interviewer: Heb jij nog iets waarvan je zegt: dat vind ik 
echt heel belangrijk om te vertellen? Daar heb ik helemaal 
niet naar gevraagd? 

00:36:26 - 00:37:54
Respondent: Wat ik heel belangrijk vind voor vrijwilligers, 
dat ze niet met een vooropgezet plan aan het werk beginnen, 
dat is gedoemd te mislukken, als je zegt, van: we gaan dit 
en dit doen. En vanuit de voorleesexpress heb je een app, 
dan kun je wat filmpjes zien en dan zie ik allemaal mooie 
dingen. Op dat filmpje staat bijvoorbeeld ook een oudere 
man, hij is vrijwilliger en hij zit voor te lezen, met de 
kinderen van een andere cultuur op schoot. Ik zeg: ik weet 
niet of je dit moet gebruiken, ik weet niet of je dat moet 
doen, want dat het gebeurt is prima, maar niet dat je dat 
als filmpje gebruikt. Dat heb ik ook tegen de coördinator 
van de VoorleesExpress gezegd. Wat ik moeilijk vind, 
als ik kijk naar
de kinderboeken, dat is mij bij mijn eigen kinderen 
nooit opgevallen, maar valt me nu op, is dat er heel veel 
boeken gaan over opa's en oma's en over feestjes, wat totaal
niet herkenbaar is voor kinderen in de gezinnen waar je 
komt. Ik moet hele andere boeken zoeken, dus dat is 
misschien meer een advies voor de kinderboekenschrijfsters. 

00:37:57 - 00:38:05
Interviewer: Ik begreep wel dat ze hier bezig zijn met het 
opbouwen van een internationale kindercollectie, dus met 
boeken vanuit de hele wereld. 

00:38:05 - 00:38:16
Respondent: Dat moeten ze ook doen, want ze hebben wel 
boeken uit Afrikaanse landen, maar dit is niet herkenbaar 
voor Chinese kinderen. 

00:38:19 - 00:38:22
Interviewer: Heb jij nog boeken meegenomen uit Jordanië? 

00:38:23 - 00:38:44
Respondent: Nee, ik heb geen boeken meegenomen, maar ik heb 
trouwens ook geen boekenzaak gezien, moet ik zeggen. Ik heb 
alleen maar die kruidenzaken en sieraden, ik heb geen 
boekenzaak gezien. Ik denk ook niet dat die gids het laat 
zien. Ze zullen er wel zijn, want Jordanië heeft een 
heleboel universiteiten. Dus daar moeten boekenzaken zijn, 
maar ik heb ze niet gezien. 

00:38:49 - 00:38:52
Interviewer: Zijn nog meer dingen waarvan je zegt: dat vind 
ik echt wel even belangrijk? 

00:38:54 - 00:39:16
Respondent: Het valt me zo niet in. Spelletjes zijn heel 
belangrijk, dat merk ik wel. Of doe spelletjes, dat ze iets 
moeten doen, iets moeten zoeken. Voor de rest valt me niet 
iets in. 

00:39:16 - 00:39:29
Interviewer: Het uur is ook weer om, dus we moeten ook gaan 
afronden. Ik heb ook wel al mijn vragen kunnen stellen die 
ik wilde stellen, dus als jij verder niks meer hebt, dan 
stel ik voor dat ik de opname ga stoppen. 

00:39:29 - 00:39:29
Respondent: Dat is prima.