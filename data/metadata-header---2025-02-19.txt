###---------------------------------------------------------------------
### This transcript was created for the VERLICHT project. When using 
### and citing this data, please use https://verlicht.one/citing
### Also see that site for more information.
###---------------------------------------------------------------------


###---------------------------------------------------------------------
### The TSSID or Time Stamped Source Identifier is an almost certainly 
### universally unique identifier for a ROCK source. Together with the 
### UID or Utterance Identifier this allows unequivocal unique reference 
### to a qualitative data fragment. It is an ISO 8601 formatted date and 
### time, including hours and minutes but omitting seconds. The 
### recommendation is to use the moment the data collection of the data
### in this started. The TSSID is always in the UTC timezone (as also 
### denoted by the Z at the end). For more information, see
### https://en.wikipedia.org/wiki/ISO_8601
###---------------------------------------------------------------------

[[ tssid = xxx ]]


###---------------------------------------------------------------------
### The interventionId is the name of the intervention the interview 
### was about, written in camelCase.
###---------------------------------------------------------------------

[[ interventionId = xxx ]]


###---------------------------------------------------------------------
### The interviewerId is the identifier of the person who conducted the 
### interview. If interviewerw do not need to be anonymous, the 
### recommendation is to use their shORCID 
### (https://shorcid.opens.science).
###---------------------------------------------------------------------

[[ interviewerId = it29gl1 ]]


###---------------------------------------------------------------------
### The coderId is the identifier of the person who coded this version 
### of the dataset. If coders do not need to be anonymous, the 
### recommendation is to use their ShORCID 
### (https://shorcid.opens.science).
###---------------------------------------------------------------------

[[ coderId = xxx ]]


###---------------------------------------------------------------------
### The URCID or Unique ROCK Codebook Identifier is a URL pointing to 
### the ROCK codebook for this data.
###---------------------------------------------------------------------

[[urcid||https://docs.google.com/spreadsheets/d/1Btn9DwRYrFfO-FqJSaeY75euDLbnDHMFaA7yFwMtfbA/export?format=xlsx]]


###---------------------------------------------------------------------

