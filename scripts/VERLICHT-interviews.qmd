---
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r}
#| label: setup

###-----------------------------------------------------------------------------
### Check for presence of the {rock} package and other required packages
###-----------------------------------------------------------------------------

if ((!(requireNamespace("rock", quietly = TRUE))) ||
      (packageVersion("rock") < "0.8")) {
  stop("You need to have at least version 0.8 of the `rock` package installed; ",
       "install it with:\n\ninstall.packages('rock');");
}

rock::checkPkgs(
  "here",               ### For easily access to files using 'relative paths'
  NULL                  ### To be able to have a comma after the previous pkg
);

### Potentially update to the development version of some packages; note that
### this requires the remotes and/or devtools packages, which aren't checked
### for by the code above.
# remotes::install_gitlab("r-packages/rock@dev");
# devtools::load_all("C:/git/R/rock");

###-----------------------------------------------------------------------------
### Set paths
###-----------------------------------------------------------------------------

basePath <- here::here();

dataPath <- file.path(basePath, "data");
outputPath <- file.path(basePath, "intermediate-results");

```


```{r-add-metadata-header-to-sources-and-rename-files-single-serving, eval=FALSE}

###-----------------------------------------------------------------------------
###
### Add metadata header to sources and rename files
###
### Note that this normally doesn't need to be executed; this is here for
### archival purposes.
###
###-----------------------------------------------------------------------------

if (FALSE) {
  ### (so this isn't executed)
  
  metadataHeader <- readLines(
    file.path(dataPath, "metadata-header---2025-02-19.txt")
  );
  
  tssids <- c();

  dirs <- list.dirs(dataPath, full.names = FALSE);
  
  dirs <- dirs[nchar(dirs) > 0];
  dirs <- dirs[dirs != "coded"];
  
  for (currentDir in dirs) {
    
    files <- list.files(
      file.path(dataPath, currentDir),
      pattern = "^[0-9].*"
    );
    
    for (currentFile in files) {
      
      tssid <- paste0(
        "20",
        gsub("([0-9]+)-.*", "\\1", currentFile),
        "T",
        gsub("[0-9]+-([0-9]{4}).*", "\\1", currentFile),
        "Z"
      );
      
      newFilename <-
        paste0(
        "tssid-",
        tssid,
        "---",
        currentDir,
        ".rock"
      );
      
      newFilename <-
        sub(
          "uid",
          "UID",
          newFilename,
          fixed = TRUE
        );
      
      fileContents <-
        readLines(
          file.path(dataPath, currentDir, currentFile)
        );
      
      newFileContents <- c(metadataHeader, fileContents);
      
      newFileContents <-
        sub(
          "[[tssid=xxx]]",
          paste0("[[tssid=", tssid, "]]"),
          newFileContents,
          fixed = TRUE
        );
      
      writeLines(
        text = newFileContents,
        con = file.path(dataPath, currentDir, newFilename)
      );
    
      tssids <- c(tssids, tssid);

    }

  }
  
  tssids <- sort(unique(tssids));
  
  openxlsx::write.xlsx(
    data.frame(tssid = tssids,
               interventionId = ""),
    file.path(dataPath, "interventionId-designation.xlsx")
  );

}


```

```{r}

###-----------------------------------------------------------------------------
### Show collected fragments
###-----------------------------------------------------------------------------

### Note: first run the top-most "setup" chunk. You run it by clicking the
### "play" button in its top-right corner.

### Change filename to file you want to extract fragments from in the
### 'data-coded' subdirectory in the 'data' subdirectory

filename <- "230403-133559--transcript--codering--GJ.rock";

test <- rock::parse_source(
  file.path(
    dataPath,
    filename
  )
);

rock::collect_coded_fragments(
  test,
  context = 5,
  output = file.path(outputPath, "collected-fragments.html")
);

```


```{r}

###-----------------------------------------------------------------------------
### Show a network
###-----------------------------------------------------------------------------

### Note: first run the top-most "setup" chunk. You run it by clicking the
### "play" button in its top-right corner.

### Change filename to file you want to test in the
### 'data-coded' subdirectory in the 'data' subdirectory

filename <- "230403-133559--transcript--codering--GJ.rock";

test <- rock::parse_source(
  file.path(
    dataPath,
    filename
  )
);

DiagrammeR::render_graph(
  test$networkCodes$network$graph
);

```


```{r}

###-----------------------------------------------------------------------------
### Extract all used code identifiers
###-----------------------------------------------------------------------------

### Note: first run the top-most "setup" chunk. You run it by clicking the
### "play" button in its top-right corner.

allCodedSources <- rock::parse_sources(
  dataPath
);

QNA_codeIds <-
  rock::get_codeIds_from_qna_codings(allCodedSources);

QNA_codeIds_combined_within_source <-
  lapply(
    QNA_codeIds,
    rock::rbind_df_list
  );

QNA_codeIds_combined <-
  rock::rbind_df_list(
    lapply(
      names(QNA_codeIds_combined_within_source),
      function(i) {
        res <- QNA_codeIds_combined_within_source[[i]];
        res$source <- i;
        return(res);
      }
    )
  );

QNA_codeIds_combined <-
  QNA_codeIds_combined[
    ,
    c("source", "uid", "codeId", "type")
  ];

QNA_codeIds_combined <-
  QNA_codeIds_combined[
    order(QNA_codeIds_combined$source,
          QNA_codeIds_combined$codeId),
  ];

openxlsx::write.xlsx(
  QNA_codeIds_combined,
  file.path(outputPath, "qna_identifiers_by_source.xlsx")
);


# QNA_codeIds_merged <-
#   rock::rbind_df_list(
#     QNA_codeIds
#   );

# writeClipboard(sort(unique(unlist(QNA_codeIds))));

```

